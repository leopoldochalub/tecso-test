package coop.tecso.examen.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Account{

	private static final long serialVersionUID = -8901155893511467206L;
	
	@Id
	private Long id;
	private Currency currency;
	@Column(columnDefinition = "DECIMAL(10,2)")
	private BigDecimal amount;
	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "account_id")
	private List<Movement> movements = new ArrayList<>();

	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public List<Movement> getMovements() {
		return movements;
	}
	public void setMovements(List<Movement> movements) {
		this.movements = movements;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	
	
	
}
