package coop.tecso.examen.model;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class AmountLimit{

	private static final long serialVersionUID = -8901155893511467206L;
	
	@Id
	private Currency currency;
	@Column(columnDefinition = "DECIMAL(10,2)")
	private BigDecimal amountLimit;

	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	public BigDecimal getAmountLimit() {
		return amountLimit;
	}
	public void setAmountLimit(BigDecimal amountLimit) {
		this.amountLimit = amountLimit;
	}
	
	
	
}
