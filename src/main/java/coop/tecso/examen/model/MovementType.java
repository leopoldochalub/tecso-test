package coop.tecso.examen.model;

public enum MovementType {

	DEBIT,
	CREDIT

}
