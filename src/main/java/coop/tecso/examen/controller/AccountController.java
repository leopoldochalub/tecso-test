package coop.tecso.examen.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import coop.tecso.examen.dto.AccountResponse;
import coop.tecso.examen.dto.CountryDto;
import coop.tecso.examen.dto.CreateAccountRequest;
import coop.tecso.examen.dto.DeleteAccountRequest;
import coop.tecso.examen.dto.GenericResponse;
import coop.tecso.examen.dto.MovementRequest;
import coop.tecso.examen.dto.MovementResponse;
import coop.tecso.examen.model.Account;
import coop.tecso.examen.model.AmountLimit;
import coop.tecso.examen.model.Currency;
import coop.tecso.examen.model.Movement;
import coop.tecso.examen.model.MovementType;
import coop.tecso.examen.repository.AccountRepository;
import coop.tecso.examen.repository.AmountLimitRepository;

@RestController
@RequestMapping("/account")
public class AccountController {

	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private AmountLimitRepository amountLimitRepository;
	
	@GetMapping("/findAll")
	public List<AccountResponse> findAll() {
		
		List<AccountResponse> result = new ArrayList<>();
		for (Account entity : accountRepository.findAll()) {
			AccountResponse accountResponse = new AccountResponse();
			accountResponse.setId(entity.getId());
			accountResponse.setCurrency(entity.getCurrency().name());
			accountResponse.setAmount(entity.getAmount());
			for (Movement movement : entity.getMovements()) {
				MovementResponse mv = new MovementResponse();
				mv.setAmount(movement.getAmount());
				mv.setDate(movement.getDate());
				mv.setDescription(movement.getDescription());
				mv.setType(movement.getType().name());
				accountResponse.getMovements().add(mv);
			}
			result.add(accountResponse);
		}
		
	    return result;
	}
	
	@PostMapping("/create")
	public ResponseEntity<?> create(@RequestBody CreateAccountRequest request) {
		
		try {
			Account entity = new Account();
			entity.setId(request.getAccountId());
			entity.setCurrency(Currency.valueOf(request.getCurrency()));
			entity.setAmount(new BigDecimal(0));
			accountRepository.save(entity);
		}catch(Exception ex) {
			GenericResponse response = new GenericResponse();
			response.setResult("ERROR");
	    	return new ResponseEntity<GenericResponse>(response, HttpStatus.BAD_REQUEST);
		}
		
		GenericResponse response = new GenericResponse();
		response.setResult("OK");
    	return new ResponseEntity<GenericResponse>(response, HttpStatus.OK);
	
	}
	
	@PostMapping("/delete")
	public ResponseEntity<?> delete(@RequestBody DeleteAccountRequest request) {
		
		Account entity = accountRepository.findById(request.getAccountId()).get();
		if(entity.getMovements().isEmpty()) {
			accountRepository.deleteById((request.getAccountId()));
			GenericResponse response = new GenericResponse();
			response.setResult("OK");
	    	return new ResponseEntity<GenericResponse>(response, HttpStatus.OK);
		}else {
			GenericResponse response = new GenericResponse();
			response.setResult("No puede eliminarse. La cuenta posee movimientos.");
	    	return new ResponseEntity<GenericResponse>(response, HttpStatus.BAD_REQUEST);
		}
	
	}
	
	@GetMapping("/listMovements")
	public List<MovementResponse> listMovements(@RequestParam Long id) {
		
		Account entity = accountRepository.findById(id).get();
		
		List<MovementResponse> result = new ArrayList<>();
		for (Movement movement : entity.getMovements()) {
			MovementResponse mv = new MovementResponse();
			mv.setAmount(movement.getAmount());
			mv.setDate(movement.getDate());
			mv.setDescription(movement.getDescription());
			mv.setType(movement.getType().name());
			result.add(mv);
		}
		
		result.sort(Comparator.comparing(MovementResponse::getDate).reversed());
		
	    return result;
	}
	
	@PostMapping("/addMovement")
    public ResponseEntity<?> addMovement(@RequestBody MovementRequest request) {
		
		Account entity = accountRepository.findById(request.getAccountId()).get();
		
		Movement movement = new Movement();
		movement.setType(MovementType.valueOf(request.getType()));
		movement.setDescription(request.getDescription());
		movement.setAmount(request.getAmount());
		movement.setDate(new Date());
		
		entity.getMovements().add(movement);
		
		if(movement.getType().equals(MovementType.CREDIT))entity.setAmount(entity.getAmount().add(movement.getAmount()));
		else entity.setAmount(entity.getAmount().subtract(movement.getAmount()));
		
		String validationMessage = validateAmount(entity.getCurrency(),entity.getAmount());	
		
		if(validationMessage!=null) {
			GenericResponse response = new GenericResponse();
			response.setResult(validationMessage);
	    	return new ResponseEntity<GenericResponse>(response, HttpStatus.BAD_REQUEST);
		}
		
		
		accountRepository.save(entity);
		
		GenericResponse response = new GenericResponse();
		response.setResult("OK");
    	return new ResponseEntity<GenericResponse>(response, HttpStatus.OK);
    
    }

	private String validateAmount(Currency currency, BigDecimal amount) {
		
		List<AmountLimit> amountLimits = amountLimitRepository.findAll();
		for (AmountLimit limit : amountLimits) {
			if(limit.getCurrency().equals(currency)) {
				if(amount.add(limit.getAmountLimit()).compareTo(new BigDecimal(0))<0)
					return "Limite de descubierto superado.";
			}
		}
		
		return null;
	}
	
}
