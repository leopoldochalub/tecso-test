package coop.tecso.examen.dto;

import java.io.Serializable;

public class CreateAccountRequest implements Serializable {

	private static final long serialVersionUID = -1737700610469175651L;

	private Long accountId;
	private String currency;
	
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	
}
