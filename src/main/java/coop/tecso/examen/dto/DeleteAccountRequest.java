package coop.tecso.examen.dto;

import java.io.Serializable;

public class DeleteAccountRequest implements Serializable {

	private static final long serialVersionUID = -1737700610469175651L;

	private Long accountId;

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	
	

}
