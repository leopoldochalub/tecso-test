package coop.tecso.examen.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class MovementRequest implements Serializable {

	private static final long serialVersionUID = -1737700610469175651L;

	private Long accountId;
	private String type;
	private BigDecimal amount;
	private String description;
	
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
