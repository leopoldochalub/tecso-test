package coop.tecso.examen.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class MovementResponse implements Serializable {
	

	private static final long serialVersionUID = -1854383574061855612L;

	private Date date;
	private String type;
	private String description;
	private BigDecimal amount;
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	

}
