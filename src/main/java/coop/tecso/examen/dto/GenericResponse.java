package coop.tecso.examen.dto;

import java.io.Serializable;

public class GenericResponse implements Serializable {
	

	private static final long serialVersionUID = -1854383574061855612L;
	
	private String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
