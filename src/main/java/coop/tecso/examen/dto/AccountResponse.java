package coop.tecso.examen.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AccountResponse implements Serializable {
	

	private static final long serialVersionUID = -1854383574061855612L;

	
	private Long id;
	private String currency;
	private BigDecimal amount;
	private List<MovementResponse> movements = new ArrayList<>();
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public List<MovementResponse> getMovements() {
		return movements;
	}
	public void setMovements(List<MovementResponse> movements) {
		this.movements = movements;
	}
	
	

}
