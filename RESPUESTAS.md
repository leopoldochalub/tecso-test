

//LISTAR CUENTAS
curl -X GET "http://localhost:8080/api/account/findAll"

//CREAR CUENTA
curl -X POST -H 'Content-Type: application/json' -H 'Accept: application/json' -d '{"accountId":1,"currency":"PESO"}' localhost:8080/api/account/create

//ELIMINAR CUENTA
curl -X POST -H 'Content-Type: application/json' -H 'Accept: application/json' -d '{"accountId":1}' localhost:8080/api/account/delete

//LISTAR MOVIMIENTOS X CUENTA
curl -X GET "http://localhost:8080/api/account/listMovements?id=1"

//AGREGAR MOVIMIENTO
curl -X POST -H 'Content-Type: application/json' -H 'Accept: application/json' -d '{"accountId":1,"type":"CREDIT","amount":555,"description":"Descripcion credito"}' localhost:8080/api/account/addMovement

curl -X POST -H 'Content-Type: application/json' -H 'Accept: application/json' -d '{"accountId":1,"type":"DEBIT","amount":555,"description":"Descripcion debito"}' localhost:8080/api/account/addMovement

